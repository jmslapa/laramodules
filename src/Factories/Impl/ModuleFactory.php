<?php

namespace Jmslapa\Laramodules\Factories\Impl;

use Jmslapa\Laramodules\Factories\IModuleFactory;
use Jmslapa\Laramodules\Models\IModule;
use Jmslapa\Laramodules\Models\Impl\Module;

class ModuleFactory implements IModuleFactory
{

    public function make(string $name): IModule
    {
        return new Module($name);
    }
}
