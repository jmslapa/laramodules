<?php

namespace Jmslapa\Laramodules\Factories;

use Jmslapa\Laramodules\Models\IModule;

interface IModuleFactory
{
    public function make(string $name): IModule;
}
