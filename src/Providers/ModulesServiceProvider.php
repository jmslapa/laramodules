<?php

namespace Jmslapa\Laramodules\Providers;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Jmslapa\Laramodules\Utils\Path;
use ReflectionClass;
use ReflectionException;
use Jmslapa\Laramodules\Facades\Modules;
use Jmslapa\Laramodules\Factories\IModuleFactory;
use Jmslapa\Laramodules\Factories\Impl\ModuleFactory;
use Jmslapa\Laramodules\Models\IModule;
use Jmslapa\Laramodules\Models\IModuleBag;
use Jmslapa\Laramodules\Models\Impl\Module;
use Jmslapa\Laramodules\Models\Impl\ModuleBag;
use Jmslapa\Laramodules\Utils\Reflection;
use Symfony\Component\Finder\SplFileInfo;

class ModulesServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->performBindings();
        $this->registerModulesProviders();
        $this->registerCommands();
    }

    public function boot()
    {
        if(!is_dir(base_path('modules'))) {
            mkdir(base_path('modules'));
        }
        $this->publishes([
            Path::resolve(__DIR__.'/../Config/modules.php') => config_path('modules.php'),
        ], 'modules-config');
        $this->loadMigrations();
        $this->mapRoutes();
    }

    private function performBindings()
    {
        $this->app->bind(IModule::class, Module::class);
        $this->app->bind(IModuleFactory::class, ModuleFactory::class);
        $this->app->singleton(IModuleBag::class, fn() => ModuleBag::getInstance());
    }

    private function registerModulesProviders()
    {
        foreach (Modules::getProviders() as $provider) {
            $this->app->register($provider);
        }
    }

    private function loadMigrations()
    {
        foreach (Modules::getPaths('Migrations') as $path) {
            $this->loadMigrationsFrom($path);
        }
    }

    private function mapRoutes()
    {
        $files = collect(Modules::getPaths('Routes'))->reduce(function (Collection $files, string $path) {
            if(File::isDirectory($path)) {
                $files->push(...File::allFiles($path));
            }
            return $files;
        }, collect());

        $files->each(function (SplFileInfo $file): void {
            if ($file->getExtension() === 'php') {
                $this->loadRoutesFrom($file->getPathname());
            }
        });
    }

    private function registerCommands(): void
    {
        $classes = Reflection::loadClassesFromDirectory(Path::resolve(__DIR__.'/../Commands'));
        $classes = array_filter($classes, function ($class) {
            try {
                $metaClass = new ReflectionClass($class);
            } catch (ReflectionException $e) {
                return false;
            }

            return ! $metaClass->isAbstract() && $metaClass->isSubclassOf(Command::class);
        });
        $this->commands($classes);
    }

}
