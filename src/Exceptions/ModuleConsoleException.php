<?php

namespace Jmslapa\Laramodules\Exceptions;

use RuntimeException;

class ModuleConsoleException extends RuntimeException
{
    public function __construct($message = "")
    {
        parent::__construct($message);
    }

}