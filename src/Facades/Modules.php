<?php

namespace Jmslapa\Laramodules\Facades;

use Illuminate\Support\Facades\Facade;
use Jmslapa\Laramodules\Models\IModule;
use Jmslapa\Laramodules\Models\IModuleBag;

/**
 * @method static string[] getModules()
 * @method static IModule getModule(string $name)
 * @method static string[] getNames()
 * @method static string[] getPaths(string $relative = null)
 * @method static string[] getProviders()
 * @method static string[] getModels()
 * @method static string[] getPolicies()
 * @method static string[] getMiddlewares()
 * @method static void refresh()
 */
class Modules extends Facade
{
    protected static function getFacadeAccessor()
    {
        return IModuleBag::class;
    }
}
