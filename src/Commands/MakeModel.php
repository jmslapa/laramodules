<?php

namespace Jmslapa\Laramodules\Commands;

use Illuminate\Support\Facades\Artisan;

class MakeModel extends ModuleMakeCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:make-model {module : The name of the module} {name : The name of the class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a model class inside a module context.';

    protected function getClassNameDefinerArgument(): string
    {
        return 'name';
    }

    protected function getOriginalDestinationPath(): string
    {
        return 'app/Models';
    }

    protected function getModuleDestinationPath(): string
    {
        return 'Models';
    }

    protected function callArtisan(): void
    {
        Artisan::call("make:model {$this->argument($this->getClassNameDefinerArgument())}");
    }

}
