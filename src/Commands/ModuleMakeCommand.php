<?php

namespace Jmslapa\Laramodules\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Jmslapa\Laramodules\Exceptions\ModuleConsoleException;
use Jmslapa\Laramodules\Facades\Modules;
use Jmslapa\Laramodules\Models\IModule;
use Symfony\Component\Finder\SplFileInfo;
use Throwable;

abstract class ModuleMakeCommand extends Command
{
    /**
     * The target module.
     *
     * @var \Jmslapa\Laramodules\Models\IModule
     */
    protected IModule $module;

    /**
     * The name of required class.
     *
     * @var null|string
     */
    protected ?string $className;

    /**
     * The namespace of required class.
     *
     * @var string
     */
    protected string $namespace;

    /**
     * The fully qualified name of required class.
     *
     * @var string
     */
    protected string $FQN;

    /**
     * The original destination path of created file.
     *
     * @var string
     */
    protected string $originalDestinationPath;

    /**
     * The module destination path of created file.
     *
     * @var string
     */
    protected string $moduleDestinationPath;

    /**
     * Exploded name argument
     *
     * @var string[]
     */
    protected array $exploded;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    abstract protected function getClassNameDefinerArgument(): string;

    abstract protected function getOriginalDestinationPath(): string;

    abstract protected function getModuleDestinationPath(): string;

    abstract protected function callArtisan(): void;

    /**
     * @throws \Throwable
     */
    protected function performMove(): void
    {
        if (File::exists($this->originalDestinationPath)) {
            if(File::exists($this->moduleDestinationPath)) {
                throw new ModuleConsoleException('File already exists.');
            }
            try {
                $exploded = explode('/', $this->argument($this->getClassNameDefinerArgument()));
                if(count($exploded) > 1  && !File::isDirectory(dirname($this->moduleDestinationPath))) {
                    $directory = array_shift($exploded);
                    File::moveDirectory(
                        dirname($this->originalDestinationPath),
                        dirname($this->moduleDestinationPath),
                    );
                } else {
                    File::move($this->originalDestinationPath, $this->moduleDestinationPath);
                }
            } catch (Throwable $th) {
                if (File::exists($this->originalDestinationPath)) {
                    File::delete($this->originalDestinationPath);
                }
                throw $th;
            }
        } else {
            throw new ModuleConsoleException("Unable to create file.");
        }
    }

    protected function getModuleInstance(): IModule
    {
        $module = Modules::getModule($this->argument('module'));
        if ( ! $module) {
            throw new ModuleConsoleException("Module not found.");
        }

        return $module;
    }

    protected function getClassName(): string
    {
        return array_pop($this->exploded);
    }

    protected function getNamespace(): string
    {
        $pseudoFQN = trim(str_replace(base_path(), '', $this->moduleDestinationPath), "/ \t\n\r\0\x0B");
        $pseudoNamespace = Str::before($pseudoFQN, DIRECTORY_SEPARATOR.$this->className);

        return ucfirst(str_replace('/', '\\', $pseudoNamespace));
    }

    protected function getFQN(): string
    {
        return "$this->namespace\\$this->className";
    }

    protected function makeOriginalDestinationPath(): string
    {
        return base_path(
            trim($this->getOriginalDestinationPath(), "/ \t\n\r\0\x0B").
            "/{$this->argument($this->getClassNameDefinerArgument())}.php"
        );
    }

    protected function makeModuleDestinationPath(): string
    {
        return $this->module->getPath(
            trim($this->getModuleDestinationPath(), "/ \t\n\r\0\x0B").
            "/{$this->argument($this->getClassNameDefinerArgument())}.php"
        );
    }

    protected function updateNamespace()
    {
        $file = $this->getFile();
        $content = $file->getContents();
        $namespaceMatches = null;
        preg_match('/namespace\s((\w+\\\)*\w+);/', $content, $namespaceMatches);
        File::put($file->getPathname(), str_replace($namespaceMatches[1], $this->namespace, $content));
    }

    protected function getFile(): SplFileInfo {
        $files = array_filter(
            File::allFiles($this->module->getPath($this->getModuleDestinationPath())),
            fn(SplFileInfo $f) => $f->getFilenameWithoutExtension() === $this->className
        );
        return array_shift($files);
    }

    protected function setUp()
    {
        $this->exploded                = explode("/", $this->argument($this->getClassNameDefinerArgument()));
        $this->module                  = $this->getModuleInstance();
        $this->originalDestinationPath = $this->makeOriginalDestinationPath();
        $this->moduleDestinationPath   = $this->makeModuleDestinationPath();
        $this->className               = $this->getClassName();
        $this->namespace               = $this->getNamespace();
        $this->FQN                     = $this->getFQN();
    }

    /**
     * @throws \Throwable
     */
    protected function executeCommand(): void
    {
        $this->setUp();
        $this->callArtisan();
        $this->performMove();
        $this->updateNamespace();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Throwable
     */
    public function handle(): int
    {
        try {
            $this->executeCommand();
            $this->info("File created with success!");
            return 0;
        } catch (ModuleConsoleException $e) {
            $this->error($e->getMessage());
            return 1;
        }
    }
}
