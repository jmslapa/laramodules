<?php

namespace Jmslapa\Laramodules\Commands;

use Illuminate\Support\Facades\Artisan;

class MakeTest extends ModuleMakeCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:make-test '.
                           '{module : The name of the module} '.
                           '{name : The name of the class} '.
                           '{--u|unit} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a controller class inside a module context.';

    protected function getClassNameDefinerArgument(): string
    {
        return 'name';
    }

    protected function getOriginalDestinationPath(): string
    {
        return $this->option('unit') ? 'tests/Unit' : 'tests/Feature';
    }

    protected function getModuleDestinationPath(): string
    {
        return $this->option('unit') ? 'Tests/Unit' : 'Tests/Feature';
    }

    protected function callArtisan(): void
    {
        $arguments = array_merge(['name' => $this->argument('name')], [
            '--unit' => $this->option('unit'),
        ]);
        Artisan::call("make:test", $arguments);
    }

}
