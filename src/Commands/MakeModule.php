<?php

namespace Jmslapa\Laramodules\Commands;

use DOMDocument;
use DOMElement;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Jmslapa\Laramodules\Exceptions\ModuleConsoleException;

class MakeModule extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:make {name : The name of the module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a module context.';

    public function makeDirectories(array $directories, string $path)
    {
        foreach ($directories as $key => $value) {
            if (is_array($value)) {
                File::makeDirectory("$path/$key");
                File::put("$path/$key/.gitkeep", '');
                $this->makeDirectories($value, "$path/$key");
            } else {
                File::makeDirectory("$path/$value");
                File::put("$path/$value/.gitkeep", '');
            }
        }
    }

    public function addTestDirectoriesToPhpunitXml(): void
    {
        if(file_exists(base_path('phpunit.xml'))) {
            $dom = new DOMDocument();
            $dom->loadXML(file_get_contents(base_path('phpunit.xml')));

            $suites = collect($dom->getElementsByTagName('testsuite'));
            $suites->each(function (DOMElement $e) use ($dom) {
                $child = $e->appendChild($dom->createElement('directory'));
                $child->setAttribute('suffix', 'Test.php');
                $child->nodeValue = "./modules/".
                                    preg_replace('/\W/', '', $this->argument('name')).
                                    "/Tests/".
                                    $e->getAttribute('name');
            });

            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($dom->saveXML());
            $xml = $dom->saveXML();
            File::put(base_path('phpunit.xml'), $xml);
        }
    }

    public function handle(): int
    {
        try {
            $name = preg_replace('/\W/', '', $this->argument('name'));
            $path = base_path("modules/$name");

            if (File::isDirectory($path)) {
                throw new ModuleConsoleException("The module already exists.");
            }

            File::makeDirectory($path);
            $this->makeDirectories(config('modules.directories', []), $path);
            $this->addTestDirectoriesToPhpunitXml();

            $this->info('Module created with success!');

            return 0;
        } catch (ModuleConsoleException $e) {
            $this->error($e->getMessage());

            return 1;
        }
    }

}
