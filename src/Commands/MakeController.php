<?php

namespace Jmslapa\Laramodules\Commands;

use Illuminate\Support\Facades\Artisan;

class MakeController extends ModuleMakeCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:make-controller '.
                           '{module : The name of the module} '.
                           '{name : The name of the class} '.
                           '{--api} '.
                           '{--r|resource} '.
                           '{--i|invokable}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a controller class inside a module context.';

    protected function getClassNameDefinerArgument(): string
    {
        return 'name';
    }

    protected function getOriginalDestinationPath(): string
    {
        return 'app/Http/Controllers';
    }

    protected function getModuleDestinationPath(): string
    {
        return 'Http/Controllers';
    }

    protected function callArtisan(): void
    {
        $arguments = array_merge(['name' => $this->argument($this->getClassNameDefinerArgument())], [
            '--api' => $this->option('api'),
            '--resource' => $this->option('resource'),
            '--invokable' => $this->option('invokable')
        ]);
        Artisan::call("make:controller", $arguments);
    }

    private function updateExtends()
    {
        $file = $this->getFile();
        $content = $file->getContents();
        $extendsMatches = null;
        preg_match('/extends\s((\w+\\\)*\w+)/', $content, $extendsMatches);
        $sentence = $extendsMatches[0];
        $updatedSentence = str_replace('Controller', '\\App\\Http\\Controllers\\Controller', $sentence);
        file_put_contents($file->getPathname(), str_replace($sentence, $updatedSentence, $content));
    }

    protected function executeCommand(): void
    {
        parent::executeCommand();
        $this->updateExtends();
    }

}
