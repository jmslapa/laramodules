<?php

namespace Jmslapa\Laramodules\Utils;

use Illuminate\Support\Facades\File;

class Reflection
{

    /**
     * @param  string  $dir
     * @param  bool  $recursive
     *
     * @return string[]
     */

    public static function loadClassesFromDirectory(string $dir, bool $recursive = true): array
    {
        $classes = [];
        if ( ! File::isDirectory($dir)) {
            return $classes;
        }

        $files = $recursive ? File::allFiles($dir) : File::files($dir);

        foreach ($files as $file) {
            if ($file->isFile() && $file->getExtension() === 'php') {
                $content = file_get_contents($file->getPathname());
                $namespaceMatches = null;
                $classMatches = null;
                preg_match('/namespace\s((\w+\\\)*\w+);/', $content, $namespaceMatches);
                preg_match('/class\s(\w+)\s/', $content, $classMatches);
                $namespace = $namespaceMatches[1] ?? null;
                $class = $classMatches[1] ?? null;
                $fqn = trim("$namespace\\$class", '\\');
                if (class_exists($fqn)) {
                    $classes[] = $fqn;
                }
            }
        }
        return $classes;
    }
}
