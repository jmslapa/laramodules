<?php


namespace Jmslapa\Laramodules\Utils;


class Path
{
    public static function resolve(string $path): string
    {
        return str_replace('/', DIRECTORY_SEPARATOR, $path);
    }
}