<?php

namespace Jmslapa\Laramodules\Models;

interface IModuleBag
{
    public static function getInstance(): IModuleBag;
    public function getModules(): array;
    public function getModule(string $name): ?IModule;
    public function getNames(): array;
    public function getPaths(string $relative = null): array;
    public function getProviders(): array;
    public function getModels(): array;
    public function getPolicies(): array;
    public function getMiddlewares(): array;
    public function refresh(): void;
}
