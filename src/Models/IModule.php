<?php

namespace Jmslapa\Laramodules\Models;

interface IModule
{
    public function getName(): string;
    public function getPath(string $relative = null): string;
    public function getProviders(): array;
    public function getModels(): array;
    public function getPolicies(): array;
    public function getMiddlewares(): array;
    public function refresh(): void;
}
