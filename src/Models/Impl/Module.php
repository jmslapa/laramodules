<?php

namespace Jmslapa\Laramodules\Models\Impl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Jmslapa\Laramodules\Models\IModule;
use Jmslapa\Laramodules\Utils\Reflection;

class Module implements IModule
{
    private string $name;
    private string $path;
    /**
     * @var string[]
     */
    private array $providers;
    /**
     * @var string[]
     */
    private array $models;
    /**
     * @var string[]
     */
    private array $policies;
    /**
     * @var string[]
     */
    private array $middlewares;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->path = base_path("modules/$name");
        $this->load();
    }

    protected function load()
    {
        $this->loadProviders();
        $this->loadModels();
        $this->loadPolicies();
        $this->loadMiddlewares();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPath(string $relative = null): string
    {
        if ($relative) {
            $relative = trim($relative, " /\t\n\r\0\x0B");
        }
        return "$this->path/$relative";
    }

    protected function loadProviders(): void
    {
        $this->providers = $this->filterClassesBySuperClass(
            Reflection::loadClassesFromDirectory($this->getPath('Providers')),
            ServiceProvider::class
        );
    }

    public function getProviders(): array
    {
        return $this->providers;
    }

    protected function loadModels(): void
    {
        $this->models = $this->filterClassesBySuperClass(
            Reflection::loadClassesFromDirectory($this->getPath('Models')),
            Model::class
        );
    }

    public function getModels(): array
    {
        return $this->models;
    }

    protected function loadPolicies(): void
    {
        $policies = Reflection::loadClassesFromDirectory($this->getPath('Policies'));
        $validPolicies = [];
        foreach ($this->getModels() as $model) {
            $modelName = last(explode('\\', $model));
            $filtered = array_filter($policies, fn($p) => str_ends_with($p, "{$modelName}Policy"));
            if (count($filtered)) {
                $validPolicies[$model] = last($filtered);
            }
        }
        $this->policies = $validPolicies;
    }

    public function getPolicies(): array
    {
        return $this->policies;
    }

    protected function loadMiddlewares(): void
    {
        $this->middlewares = Reflection::loadClassesFromDirectory($this->getPath('Http/Middlewares'));
    }

    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

    public function refresh(): void
    {
        $this->load();
    }

    /**
     * @param string[] $classes
     * @param string $super
     *
     * @return string[]
     */
    private function filterClassesBySuperClass(array $classes, string $super): array
    {
        return array_filter($classes, fn($class) => is_subclass_of($class, $super));
    }
}
