<?php

namespace Jmslapa\Laramodules\Models\Impl;

use Illuminate\Support\Facades\File;
use Jmslapa\Laramodules\Factories\IModuleFactory;
use Jmslapa\Laramodules\Models\IModule;
use Jmslapa\Laramodules\Models\IModuleBag;

class ModuleBag implements IModuleBag
{
    private static ?IModuleBag $instance = null;
    private IModuleFactory $factory;
    /**
     * @var IModule[]
     */
    private array $modules;

    private function __construct(IModuleFactory $factory)
    {
        $this->factory = $factory;
        $this->loadModules();
    }

    public static function getInstance(): IModuleBag
    {
        if (self::$instance === null) {
            self::$instance = new ModuleBag(app(IModuleFactory::class));
        }
        return self::$instance;
    }

    protected function loadModules(): void
    {
        $moduleNames = array_map(
            fn($d) => last(explode(DIRECTORY_SEPARATOR, $d)),
            File::isDirectory(base_path('modules')) ? File::directories(base_path('modules')) : []
        );
        $this->modules = array_map(fn($n) => $this->factory->make($n), $moduleNames);
    }

    /**
     * @return IModule[]
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    public function getModule(string $name): ?IModule
    {
        $modules = array_filter(
            $this->getModules(),
            fn(IModule $m) => $m->getName() === $name
        );

        return count($modules) ? last($modules) : null;
    }

    /**
     * @return string[]
     */
    public function getNames(): array
    {
        return array_reduce(
            $this->getModules(),
            fn(array $acc, IModule $curr) => [...$acc, $curr->getName()],
            []
        );
    }

    /**
     * @param string|null $relative
     * @return string[]
     */
    public function getPaths(string $relative = null): array
    {
        return array_reduce(
            $this->getModules(),
            fn(array $acc, IModule $curr) => [...$acc, $curr->getPath($relative)],
            []
        );
    }

    /**
     * @return string[]
     */
    public function getProviders(): array
    {
        return array_reduce(
            $this->getModules(),
            fn(array $acc, IModule $curr) => [...$acc, ...$curr->getProviders()],
            []
        );
    }

    /**
     * @return string[]
     */
    public function getModels(): array
    {
        return array_reduce(
            $this->getModules(),
            fn(array $acc, IModule $curr) => [...$acc, ...$curr->getModels()],
            []
        );
    }

    /**
     * @return string[]
     */
    public function getPolicies(): array
    {
        return array_reduce(
            $this->getModules(),
            fn(array $acc, IModule $curr) => array_merge($acc, $curr->getPolicies()),
            []
        );
    }

    /**
     * @return string[]
     */
    public function getMiddlewares(): array
    {
        return array_reduce(
            $this->getModules(),
            fn(array $acc, IModule $curr) => [...$acc, ...$curr->getMiddlewares()],
            []
        );
    }

    public function refresh(): void
    {
        $this->loadModules();
    }
}
