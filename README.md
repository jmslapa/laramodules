# Laramodules
## _A library for building a very decoupled modular Laravel application_

# How it works?
The idea behind this library is to make your modules really decoupled from the core of the application. This means that your modules can be removed from the source directory or added and your application will either absorb or leave out any features implemented within the module, like real plug-ins. Keeping this assumption in mind, ModulesServiceProvider was conceived to automatically load any providers, route files and migrations contained in the standard directories, that is, just implement, register via service provider and the integration to the core should be automatic. However, of course, the level of coupling between modules will depend directly on your code.

# Instalation
```
composer require jmslapa/laramodules
php artisan vendor:publish --provider=Jmslapa\Laramodules\Providers\ModulesServiceProvider
```

# Composer.json
Add this entry on project's composer.json psr-4 section:
```json
"autoload": {
    "psr-4": {
        "App\\": "app/",
        "Modules\\": "modules/"
    }
},
```

# Create a new module
```
php artisan module:make <module name>
```

# Initial module directory structure, defined on _config/modules.php_
```php
<?php

return [
    'directories' => [
        'Http' => [
            'Controllers'
        ],
        'Migrations',
        'Models',
        'Providers',
        'Repositories',
        'Services',
        'Routes',
        'Tests' => [
            'Feature',
            'Unit'
        ],
    ]
];
```
