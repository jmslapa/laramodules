<?php


namespace Jmslapa\Laramodules\Tests;


use Illuminate\Support\Facades\Artisan;
use Mockery;

abstract class ModuleMakeCommandTestCase extends TestCase
{
    protected Mockery\MockInterface $command;

    protected function setUp(): void
    {
        parent::setUp();

        $this->command = Mockery::mock($this->targetCommandClass())
                                ->makePartial()
                                ->shouldAllowMockingProtectedMethods();
    }

    public abstract function targetCommandClass(): string;

    public abstract function expectedClassNameDefinerArgument(): string;

    public abstract function expectedClassNameDefinerArgumentValue(): string;

    public abstract function expectedOriginalDestinationPath(): string;

    public abstract function expectedModuleDestinationPath(): string;

    public abstract function expectedArtisanCall(): string;

    /**
     * @throws \ReflectionException
     */public function testGetClassNameDefinerArgument()
    {
        self::assertEquals(
            $this->expectedClassNameDefinerArgument(),
            self::getMethod($this->command, 'getClassNameDefinerArgument')->invoke($this->command)
        );
    }

    /**
     * @throws \ReflectionException
     */public function testGetOriginalDestinationPath()
    {
        self::assertEquals(
            $this->expectedOriginalDestinationPath(),
            self::getMethod($this->command, 'getOriginalDestinationPath')->invoke($this->command)
        );
    }

    /**
     * @throws \ReflectionException
     */public function testGetModuleDestinationPath()
    {
        self::assertEquals(
            $this->expectedModuleDestinationPath(),
            self::getMethod($this->command, 'getModuleDestinationPath')->invoke($this->command)
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testCallArtisan()
    {
        $this->command->shouldReceive('getClassNameDefinerArgument')
                      ->once()
                      ->andReturn($this->expectedClassNameDefinerArgument());
        $this->command->shouldReceive('argument')
                      ->once()
                      ->with($this->expectedClassNameDefinerArgument())
                      ->andReturn($this->expectedClassNameDefinerArgumentValue());
        Artisan::shouldReceive('call')
               ->once()
               ->with($this->expectedArtisanCall());

        self::getMethod($this->command, 'callArtisan')
            ->invoke($this->command);
    }
}