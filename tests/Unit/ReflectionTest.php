<?php

namespace Jmslapa\Laramodules\Tests\Unit;

use Jmslapa\Laramodules\Utils\Path;
use Jmslapa\Laramodules\Utils\Reflection;
use Orchestra\Testbench\TestCase;

class ReflectionTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

        $class1 = <<<CLASS
        <?php
        
        namespace Jmslapa\Laramodules\ReflectionTestStubs;
        
        class StubClass
        {
        }
        
        CLASS;

        $class2 = <<<CLASS
        <?php
        
        namespace Jmslapa\Laramodules\ReflectionTestStubs\Sub;
        
        class StubClass
        {
        }
        
        CLASS;

        $genericFile = <<<FILE
        <?php
        
        return [
            'first-element'
        ];
        
        FILE;

        mkdir(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/Sub'), 0777, true);

        file_put_contents(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/StubClass.php'), $class1);
        file_put_contents(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/Sub/StubClass.php'), $class2);
        file_put_contents(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/generic_file.php'), $genericFile);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unlink(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/Sub/StubClass.php'));
        unlink(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/StubClass.php'));
        unlink(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/generic_file.php'));
        rmdir(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs/Sub'));
        rmdir(Path::resolve(__DIR__.'/../../src/ReflectionTestStubs'));
    }

    /**
     * @test
     */
    public function shouldReflectionReturnsAllClassesOnADirectory()
    {
        $classes = Reflection::loadClassesFromDirectory(
            Path::resolve(__DIR__.'/../../src/ReflectionTestStubs'), false
        );
        $expected = [
            'Jmslapa\Laramodules\ReflectionTestStubs\StubClass'
        ];
        self::assertEquals($expected, $classes);
    }

    /**
     * @test
     */
    public function shouldReflectionReturnsAllClassesOnADirectoryRecursively()
    {
        $classes = Reflection::loadClassesFromDirectory(
            Path::resolve(__DIR__.'/../../src/ReflectionTestStubs')
        );
        $expected = [
            'Jmslapa\Laramodules\ReflectionTestStubs\StubClass',
            'Jmslapa\Laramodules\ReflectionTestStubs\Sub\StubClass'
        ];
        self::assertEquals($expected, $classes);
    }

}
