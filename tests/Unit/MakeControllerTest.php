<?php

namespace Jmslapa\Laramodules\Tests\Unit;

use Illuminate\Support\Facades\Artisan;
use Jmslapa\Laramodules\Commands\MakeController;
use Jmslapa\Laramodules\Tests\ModuleMakeCommandTestCase;
use Mockery;

class MakeControllerTest extends ModuleMakeCommandTestCase
{

    public function targetCommandClass(): string
    {
        return MakeController::class;
    }

    public function expectedClassNameDefinerArgument(): string
    {
        return 'name';
    }

    public function expectedClassNameDefinerArgumentValue(): string
    {
        return 'ClassName';
    }

    public function expectedOriginalDestinationPath(): string
    {
        return 'app/Http/Controllers';
    }

    public function expectedModuleDestinationPath(): string
    {
        return 'Http/Controllers';
    }

    public function expectedArtisanCall(): string
    {
        return 'make:controller';
    }

    public function testCallArtisan()
    {
        $this->command->shouldReceive('option')
                      ->times(3)
                      ->with(Mockery::anyOf('api', 'resource', 'invokable'))
                      ->andReturnFalse();
        $this->command->shouldReceive('getClassNameDefinerArgument')
                      ->once()
                      ->andReturn($this->expectedClassNameDefinerArgument());
        $this->command->shouldReceive('argument')
                      ->once()
                      ->with($this->expectedClassNameDefinerArgument())
                      ->andReturn($this->expectedClassNameDefinerArgumentValue());
        Artisan::shouldReceive('call')
               ->once()
               ->with($this->expectedArtisanCall(), [
                   'name' => $this->expectedClassNameDefinerArgumentValue(),
                   '--api' => false,
                   '--resource' => false,
                   '--invokable' => false
               ]);

        self::getMethod($this->command, 'callArtisan')
            ->invoke($this->command);
    }


}
