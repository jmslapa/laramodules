<?php

namespace Jmslapa\Laramodules\Tests\Unit;

use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Jmslapa\Laramodules\Commands\ModuleMakeCommand;
use Jmslapa\Laramodules\Exceptions\ModuleConsoleException;
use Jmslapa\Laramodules\Facades\Modules;
use Jmslapa\Laramodules\Models\IModule;
use Jmslapa\Laramodules\Tests\TestCase;
use Mockery;
use Mockery\MockInterface;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Finder\SplFileInfo;
use Throwable;

class ModuleMakeCommandTest extends TestCase
{

    /**
     * @var \Jmslapa\Laramodules\Commands\ModuleMakeCommand|mixed|\Mockery\LegacyMockInterface|\Mockery\MockInterface
     */
    private $command;

    public function handleDataProvider(): array
    {
        return [
            'shouldInvokeTheExecuteCommandSelfMethodAndTheInfoSelfMethodWithASuccessMessage'              => [
                'throwsException' => false,
            ],
            'shouldInvokeTheExecuteCommandSelfMethodAndTheErrorSelfMethodWithTheMessageOfCaughtException' => [
                'throwsException' => true,
            ],
        ];
    }

    public function performMoveDataProvider(): array
    {
        return [
            'Should throw a ModuleConsoleException when file not exists on original destination  path'        => [
                'throwsException'  => true,
                'exception'        => ModuleConsoleException::class,
                'exceptionMessage' => 'Unable to create file.',
            ],
            'Should throw a ModuleConsoleException when file already exists on module destination path'       => [
                'throwsException'  => true,
                'exception'        => ModuleConsoleException::class,
                'exceptionMessage' => 'File already exists.',
            ],
            'Should search and delete if the file exists on original path and rethrow a caught exception '.
            'when an exception is thrown during the move execution'                                           => [
                'throwsException' => true,
            ],
            'Should move the full directory when the class name definer argument passed by the user is a URI' => [
                'throwsException'  => false,
                'exception'        => null,
                'exceptionMessage' => null,
                'isURI'            => true,
            ],
            'Should move the file when the class name definer argument is a simple basename'                  => [
                'throwsException' => false,
            ],
        ];
    }

    public function getModuleInstance(): array
    {
        return [
            'Should throw a ModuleConsoleException when module not exists' => [
                'moduleExists' => false,
            ],
            'Should return a IModule instance when module not exists'      => [
                'moduleExists' => true,
            ],
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->command = Mockery::mock(
            ModuleMakeCommand::class,
            fn(MockInterface $mock) => $mock->makePartial()->shouldAllowMockingProtectedMethods()
        );
    }

    /**
     * @param  array  $mockedMethods
     *
     * @return \Jmslapa\Laramodules\Commands\ModuleMakeCommand|\PHPUnit\Framework\MockObject\MockObject
     */
    public function getCommandInstance(array $mockedMethods = [])
    {
        return $this->getMockBuilder(ModuleMakeCommand::class)
                    ->enableOriginalConstructor()
                    ->onlyMethods($mockedMethods)
                    ->getMockForAbstractClass();
    }

    /**
     * @throws \ReflectionException
     */
    public function testUpdateNamespace()
    {
        $fileContent = <<<FILE
        <?php
        
        namespace NamespaceTeste;
        
        class ClassTeste
        {
        }
        FILE;

        $mockPath          = base_path('ClassTeste.php');
        $expectedNamespace = 'ExpectedNamespace';

        $file = Mockery::mock(SplFileInfo::class, function (MockInterface $mock) use ($mockPath, $fileContent) {
            $mock->shouldReceive('getContents')->once()->andReturn($fileContent);
            $mock->shouldReceive('getPathname')->once()->andReturn($mockPath);
        });

        $this->command->shouldReceive('getFile')->once()->andReturn($file);
        $namespace = self::getProperty($this->command, 'namespace');
        $namespace->setValue($this->command, $expectedNamespace);

        File::shouldReceive('put')
            ->once()
            ->with($mockPath, Mockery::on(fn($arg) => Str::contains($arg, "namespace $expectedNamespace;")))
            ->andReturn(true);

        self::getMethod($this->command, 'updateNamesPace')->invoke($this->command);
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetClassName()
    {
        $exploded = self::getProperty($this->command, 'exploded');
        $exploded->setValue($this->command, ['Teste', 'ClassTeste']);
        self::assertEquals('ClassTeste', self::getMethod($this->command, 'getClassName')->invoke($this->command));
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetFile()
    {
        $path      = 'anyPath';
        $className = 'ClassTeste';
        $file      = Mockery::mock(SplFileInfo::class, function (MockInterface $mock) use ($className) {
            $mock->shouldReceive('getFilenameWithoutExtension')->once()->andReturn($className);

            return $mock;
        });

        $module = Mockery::mock(IModule::class, function (MockInterface $mock) use ($path) {
            $mock->shouldReceive('getPath')->once()->withAnyArgs()->andReturn($path);

            return $mock;
        });

        $this->command->shouldReceive('getModuleDestinationPath')->once()->andReturn('stub');
        self::getProperty($this->command, 'className')->setValue($this->command, $className);
        self::getProperty($this->command, 'module')->setValue($this->command, $module);
        File::shouldReceive('allFiles')->once()->with($path)->andReturn([
            $file,
        ]);

        self::assertEquals($file, self::getMethod($this->command, 'getFile')->invoke($this->command));
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetNamespace()
    {
        self::getProperty($this->command, 'moduleDestinationPath')->setValue(
            $this->command, base_path('modules/Teste/Models')
        );
        self::getProperty($this->command, 'className')->setValue(
            $this->command, base_path('TesteModel')
        );

        $expected = 'Modules\Teste\Models';

        self::assertEquals($expected, self::getMethod($this->command, 'getNamespace')->invoke($this->command));
    }

    /**
     * @throws \ReflectionException
     */
    public function testSetUp()
    {
        $stubs = [
            'exploded'                => ['TestClass'],
            'module'                  => Mockery::mock(IModule::class),
            'originalDestinationPath' => 'origPath',
            'moduleDestinationPath'   => 'modulePath',
            'className'               => 'TestClass',
            'namespace'               => 'Modules\Test',
            'FQN'                     => 'Modules\Test\TestClass',
        ];

        $this->command->shouldReceive('getClassNameDefinerArgument')->once()->andReturn('name');
        $this->command->shouldReceive('argument')->once()->with('name')->andReturn('TestClass');
        $this->command->shouldReceive('getModuleInstance')->once()->andReturn($stubs['module']);
        $this->command->shouldReceive('getClassName')->once()->andReturn($stubs['className']);
        $this->command->shouldReceive('getNamespace')->once()->andReturn($stubs['namespace']);
        $this->command->shouldReceive('getFQN')->once()->andReturn($stubs['FQN']);
        $this->command->shouldReceive('makeOriginalDestinationPath')
                      ->once()
                      ->andReturn($stubs['originalDestinationPath']);
        $this->command->shouldReceive('makeModuleDestinationPath')
                      ->once()
                      ->andReturn($stubs['moduleDestinationPath']);

        self::getMethod($this->command, 'setUp')->invoke($this->command);

        $props = array_reduce(
            (new ReflectionClass($this->command))->getProperties(),
            function (array $current, ReflectionProperty $prop) use ($stubs) {
                if (in_array($prop->getName(), array_keys($stubs))) {
                    $prop->setAccessible(true);
                    $current[$prop->getName()] = $prop->getValue($this->command);
                }

                return $current;
            },
            [],
        );

        self::assertEquals(collect($stubs)->sortKeys(), collect($props)->sortKeys());
    }

    /**
     * @throws \ReflectionException
     */
    public function testExecuteCommand()
    {
        $this->command->shouldReceive('setUp')->once();
        $this->command->shouldReceive('callArtisan')->once();
        $this->command->shouldReceive('performMove')->once();
        $this->command->shouldReceive('updateNamespace')->once();

        self::getMethod($this->command, 'executeCommand')->invoke($this->command);
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetFQN()
    {
        self::getProperty($this->command, 'namespace')->setValue($this->command, 'Modules\Teste');
        self::getProperty($this->command, 'className')->setValue($this->command, 'TesteClass');
        self::assertEquals(
            'Modules\Teste\TesteClass',
            self::getMethod($this->command, 'getFQN')->invoke($this->command)
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testMakeOriginalDestinationPath()
    {
        $expected = base_path("app/Models/ReceivedNameArgument.php");

        $this->command->shouldReceive('getOriginalDestinationPath')->once()->andReturn('/app/Models/  ');
        $this->command->shouldReceive('getClassNameDefinerArgument')->once()->andReturn('name');
        $this->command->shouldReceive('argument')->once()->andReturn('ReceivedNameArgument');

        self::assertEquals(
            $expected,
            self::getMethod($this->command, 'makeOriginalDestinationPath')->invoke($this->command)
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testMakeModuleDestinationPath()
    {
        $expected = base_path("modules/Teste/Models/TesteClass.php");

        $this->command->shouldReceive('getModuleDestinationPath')->once()->andReturn('/Models/  ');
        $this->command->shouldReceive('getClassNameDefinerArgument')->once()->andReturn('name');
        $this->command->shouldReceive('argument')->once()->andReturn('TesteClass');

        $module = Mockery::mock(IModule::class, function (MockInterface $mock) use ($expected) {
            $mock->shouldReceive('getPath')
                 ->once()
                 ->with(Mockery::type('string'))
                 ->andReturnUsing(fn() => $expected);

            return $mock;
        });

        self::getProperty($this->command, 'module')->setValue($this->command, $module);

        self::assertEquals(
            $expected,
            self::getMethod($this->command, 'makeModuleDestinationPath')->invoke($this->command)
        );
    }

    /**
     * @param  bool  $throwsException
     *
     * @throws \Throwable
     * @dataProvider handleDataProvider
     */
    public function testHandle(bool $throwsException)
    {
        if ($throwsException) {
            $exceptionMessage = 'Exception message';
            $this->command->shouldReceive('executeCommand')
                          ->once()
                          ->andThrow(ModuleConsoleException::class, $exceptionMessage);
            $this->command->shouldReceive('error')->once()->with($exceptionMessage);
        } else {
            $this->command->shouldReceive('executeCommand')->once()->withNoArgs();
            $this->command->shouldReceive('info')->once()->with(Mockery::type('string'));
        }

        self::assertEquals(
            $throwsException ? 1 : 0,
            $this->command->handle()
        );
    }

    /**
     * @dataProvider performMoveDataProvider
     * @throws \ReflectionException
     */
    public function testPerformMove(
        bool $throwsException,
        ?string $exception = null,
        ?string $exceptionMessage = null,
        ?bool $isURI = false
    ) {
        $this->command->shouldReceive('getClassNameDefinerArgument')->andReturn('name');
        $this->command->shouldReceive('argument')->with('name')->andReturn($isURI ? 'Sub/Class' : 'Class');
        $originalPath = base_path($isURI ? 'app/Models/Sub/Class.php' : 'app/Models/Class.php');
        $modulePath   = base_path($isURI ? 'modules/Teste/Models/Sub/Class.php' : 'modules/Teste/Models/Class.php');
        self::getProperty($this->command, 'originalDestinationPath')->setValue($this->command, $originalPath);
        self::getProperty($this->command, 'moduleDestinationPath')->setValue($this->command, $modulePath);

        if ($throwsException) {
            if ($exception !== null) {
                if ($exceptionMessage === 'Unable to create file.') {
                    File::shouldReceive('exists')->with(Mockery::type('string'))->andReturnFalse();
                } else {
                    File::shouldReceive('exists')->with(Mockery::type('string'))->andReturnTrue();
                }
            } else {
                File::shouldReceive('exists')->with(Mockery::type('string'))->andReturnUsing(
                    fn(string $arg) => $arg === $originalPath
                );
                File::shouldReceive('move')->with($originalPath, $modulePath)->andThrow(Exception::class);
                File::shouldReceive('delete')->with($originalPath);
            }
            $this->expectException($exception ?? Throwable::class);
            $this->expectExceptionMessage($exceptionMessage ?? '');
        } else {
            File::shouldReceive('exists')->with(Mockery::type('string'))->andReturnUsing(
                fn(string $arg) => $arg === $originalPath
            );
            if ($isURI) {
                File::shouldReceive('moveDirectory')->with(dirname($originalPath), dirname($modulePath));
            } else {
                File::shouldReceive('move')->once()->with($originalPath, $modulePath);
            }
        }

        try {
            self::getMethod($this->command, 'performMove')->invoke($this->command);
        } catch (\Mockery\Exception\NoMatchingExpectationException $e) {
            dd($e);
        }
    }

    /**
     * @dataProvider getModuleInstance
     * @throws \ReflectionException
     */
    public function testGetModuleInstance(bool $moduleExists)
    {
        $moduleName = 'Test';
        $this->command->shouldReceive('argument')->with('module')->andReturn($moduleName);
        Modules::shouldReceive('getModule')->with($moduleName)->andReturn(
            $moduleExists ? Mockery::mock(IModule::class) : null
        );

        if ( ! $moduleExists) {
            $this->expectException(ModuleConsoleException::class);
            $this->expectExceptionMessage('Module not found');
        }
        $result = self::getMethod($this->command, 'getModuleInstance')->invoke($this->command);

        if ($moduleExists) {
            self::assertNotNull($result);
            self::assertInstanceOf(IModule::class, $result);
        }
    }

}
