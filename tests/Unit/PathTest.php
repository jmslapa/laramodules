<?php

namespace Jmslapa\Laramodules\Tests\Unit;

use Jmslapa\Laramodules\Utils\Path;
use PHPUnit\Framework\TestCase;

class PathTest extends TestCase
{

    /**
     * @test
     */
    public function shouldResolveReturnThePathWithTheCorrectDirectorySeparator()
    {
        $ds = DIRECTORY_SEPARATOR;
        $path = '../../src';
        $expectedPath = "..$ds..{$ds}src";

        self::assertEquals($expectedPath, Path::resolve($path));
    }

}
