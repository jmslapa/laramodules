<?php

namespace Jmslapa\Laramodules\Tests\Unit;

use Jmslapa\Laramodules\Commands\MakeModel;
use Jmslapa\Laramodules\Tests\ModuleMakeCommandTestCase;

class MakeModelTest extends ModuleMakeCommandTestCase
{

    public function targetCommandClass(): string
    {
        return MakeModel::class;
    }

    public function expectedClassNameDefinerArgument(): string
    {
        return 'name';
    }

    public function expectedClassNameDefinerArgumentValue(): string
    {
        return 'ClassName';
    }

    public function expectedOriginalDestinationPath(): string
    {
        return 'app/Models';
    }

    public function expectedModuleDestinationPath(): string
    {
        return 'Models';
    }

    public function expectedArtisanCall(): string
    {
        return 'make:model ClassName';
    }

}
