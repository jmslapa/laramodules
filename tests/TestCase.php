<?php


namespace Jmslapa\Laramodules\Tests;


use Composer\Autoload\ClassLoader;
use Illuminate\Support\Facades\File;
use Jmslapa\Laramodules\Facades\Modules;
use Jmslapa\Laramodules\Providers\ModulesServiceProvider;
use Jmslapa\Laramodules\Utils\Path;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

class TestCase extends \Orchestra\Testbench\TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        (new ClassLoader)->addPsr4('Modules\\', base_path('modules'));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        File::deleteDirectories(base_path('modules'));
    }

    protected function defineEnvironment($app)
    {
        $modulesConfig = require(Path::resolve(__DIR__.'/../src/Config/modules.php'));

        $app['config']->set('modules.directories', $modulesConfig['directories']);
    }

    protected function getPackageProviders($app)
    {
        return [
            ModulesServiceProvider::class
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Modules' => Modules::class
        ];
    }

    /**
     * @param  object  $object
     * @param  string  $method
     *
     * @return \ReflectionMethod
     * @throws \ReflectionException
     */
    public static function getMethod(object $object, string $method): ReflectionMethod
    {
        $meta = new ReflectionClass($object);
        $method = $meta->getMethod($method);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * @param  object  $object
     * @param  string  $property
     *
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public static function getProperty(object $object, string $property): ReflectionProperty
    {
        $meta = new ReflectionClass($object);
        $property = $meta->getProperty($property);
        $property->setAccessible(true);
        return $property;
    }

}